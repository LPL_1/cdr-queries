select link_event_id as encEventId
from semantic_link_slement a, semantic_link b
where a.event_id = b.event_id
and a.semantic_linkord = b.semantic_link_ord
and b.relationship_ncid = 205654 --- link to encounter
and a.link_event_version=1
and a.event_id = :problem_event_id